import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    id: slide

    title: "Integration with QPainter"
    contentWidth: codeSection.x
    content: [
        "class QSGPaintedItem",
        "Renders with QPainter",
        " QImage based",
        " FBO based",
        "Equivalent API to QDeclarativeItem"
    ]

    CodeSection {
        id: codeSection
        text:
"
class Painter : public QSGPaintedItem
{
    Q_OBJECT
public:

    void paint(QPainter *p) {
        ...
        p->drawStuff();
    }

};


// Somewhere during setup...
qmlRegisterType<Painter>(\"QtQuick\",
                         2, 0,
                         \"Painter\");

"
    }

}
