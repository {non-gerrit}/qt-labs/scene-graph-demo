import QtQuick 2.0

Rectangle
{
    id: root

    property string text
    property real fontSize: parent.baseFontSize / 2

    gradient: Gradient {
        GradientStop { position: 0; color: Qt.rgba(1, 1, 1, 0.3) }
        GradientStop { position: 1; color: Qt.rgba(0, 0, 0, 0.3) }
    }

    border.color: "lightsteelblue"
    border.width: 2
    radius: 10

    x: parent.width / 2
    width: parent.width / 2
    height: parent.height

    Item {
        anchors.fill: parent
        anchors.margins: 20
        clip: true

        Text {
            id: textItem
            anchors.fill: parent
            text: root.text;
            font.family: "courier"
            font.pixelSize: root.fontSize
            color: "white"
        }
    }
}
