import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide
{
    title: "Threaded Rendering Model"
    contentWidth: renderThreadingImage.x
    content: [
        "Dedicated render thread",
        "Animations and application logic while rendering happens in render thread"
    ]

    Image {
        id: renderThreadingImage
        source: "images/render-threading.png"
        anchors.right: parent.right
        height: parent.height
        fillMode: Image.PreserveAspectFit
        smooth: true
    }
}



//    Slide
//    {
//        title: "Traditional Rendering Model"
//        content: [
//            "One Thread",
//            "Sequential processing",
//            "Blocked during swap"
//        ]


//        Image {
//            source: "images/traditional-threading.png"
//            anchors.right: parent.right
//            anchors.rightMargin: parent.baseFontSize * 2
//            height: parent.height
//            fillMode: Image.PreserveAspectFit
//            smooth: true
//        }
//    }
