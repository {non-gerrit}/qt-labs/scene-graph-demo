import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    id: textSlide

    property real zoomOpacity: 0

    Rectangle {
        id: grabSource
        anchors.centerIn: parent
        width: parent.width / 8
        height: parent.height / 8

        border.color: Qt.rgba(1, 1, 1, textSlide.zoomOpacity);
        border.width: 2

        color: Qt.rgba(0, 0, 0, textSlide.zoomOpacity);

        Text {
            id: textElement
            anchors.centerIn: parent
            width: textSlide.width / 2
            height: textSlide.baseFontSize * 4
            wrapMode: Text.Wrap
            color: "white"
            font.pixelSize: textSlide.baseFontSize * 0.5
            font.family: "Times New Roman"

            property string content: "The default implementation of the Text element uses a technique called 'Signed Distance Fields'. This technique allows us to rasterize a glyph once and use it for any font size. It also means that we can position glyphs at arbitrary sub-pixel positions giving us true subpixel antialiasing. We combine this with unhinted layouts, so text can be freely translated, scaled and rotated in a floating-point world."
            text: content.substring(0, textElement.contentLength);

            property int contentLength: 1
            SequentialAnimation on contentLength {
                NumberAnimation { to: textElement.content.length; duration: textElement.content.length * 25; }
                PauseAnimation { duration: 5000 }
                NumberAnimation { to: textElement.content.length; duration: textElement.content.length * 25; }
                running: textSlide.visible
                loops: Animation.Infinite
            }


            SequentialAnimation on rotation {
                PauseAnimation { duration: 12000 }
                NumberAnimation { from: 0; to: 360; duration: 20000; easing.type: Easing.InOutQuart }

                running: textSlide.visible;
                loops: Animation.Infinite
            }

            SequentialAnimation on scale {
                PauseAnimation { duration: 12000 }
                NumberAnimation { to: 2; duration: 500; easing.type: Easing.InOutSine }
                PauseAnimation { duration: 2000 }
                NumberAnimation { to: 10; duration: 800; easing.type: Easing.InOutSine}
                NumberAnimation { to: 0.5; duration: 800; easing.type: Easing.InOutSine }
                NumberAnimation { to: 5; duration: 800; easing.type: Easing.InOutSine }
                PauseAnimation { duration: 2000 }
                NumberAnimation { to: 1; duration: 800; easing.type: Easing.InOutSine }

                running: textSlide.visible
                loops: Animation.Infinite
            }
        }
    }

    SequentialAnimation on zoomOpacity {
        PauseAnimation { duration: 11000 }
        NumberAnimation { to: 1; duration: 1000; easing.type: Easing.InOutQuad }
        running: textSlide.visible
    }

    ShaderEffectItem {
        opacity: textSlide.zoomOpacity
        x: textSlide.masterWidth - width - parent.x
        y: textSlide.masterHeight - height - parent.y

        onXChanged: print(x, textSlide.masterWidth, width)

        width: parent.width / 2
        height: parent.height / 2
        property variant source: ShaderEffectSource { sourceItem: grabSource }
    }
}

