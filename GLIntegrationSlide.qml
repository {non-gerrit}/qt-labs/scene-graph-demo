import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide { // GL Over / GL Under
    title: "Integration with OpenGL"
    content: [
        "Hook in custom OpenGL rendering code",
        "Under QML",
        "Over QML",
        "Inside QML using an FBO"
            ]
    contentWidth: integrationImage.x

    Image {
        id: integrationImage
        anchors.right: parent.right
        height: parent.height / 2
        smooth: true
        fillMode: Image.PreserveAspectFit
        source: "images/ogre.png"
    }

    Text {
        font.pixelSize: parent.baseFontSize * 0.4
        color: "white"
        text: "Ogre3D inside the QML Scene Graph"
        anchors.top: integrationImage.bottom
        anchors.horizontalCenter: integrationImage.horizontalCenter
    }
}

