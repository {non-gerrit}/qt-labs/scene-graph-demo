import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide {
    title: "ShaderEffectItem"
    content: [
        "Inline GLSL",
        "One or more QML items or subtrees as sources"
    ]
    contentWidth: parent.width / 3
    CodeSection {
        x: parent.width / 3 + parent.baseFontSize
        width: 2 * parent.width / 3 - parent.baseFontSize
        text: "
ShaderEffectItem {
width: 180
height: 180
property variant source: theSource
property color tint: \"red\"
fragmentShader: \"
    uniform sampler2D source;
    uniform lowp vec4 tint;
    uniform lowp float qt_Opacity;
    varying highp vec2 qt_TexCoord0;
    void main() {
        lowp vec4 c = texture2D(source, qt_TexCoord0);
        lowp float lo = min(min(c.x, c.y), c.z);
        lowp float hi = max(max(c.x, c.y), c.z);
        gl_FragColor = qt_Opacity * vec4(mix(vec3(lo), vec3(hi), tint.xyz), c.w);
    }\"
"
    }
}
