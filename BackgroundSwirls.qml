import QtQuick 2.0
import QtQuick.Particles 2.0

Item {
    anchors.fill: parent

    Rectangle {
        anchors.fill: parent
        gradient: Gradient {
                        GradientStop { position: 0; color: "#355f80" }
            //GradientStop { position: 0; color: "steelblue" }
            GradientStop { position: 1; color: "black" }
        }
    }

    Rectangle {
        id: colorTableItem
        width: 16
        height: 64
        anchors.fill: parent

        property color color1: Qt.rgba(0.8, 0.8, 1, 0.2)
        property color color2: Qt.rgba(0.8, 0.8, 1, 0.2)

        gradient: Gradient {
            GradientStop { position: 0; color: "transparent" }
            GradientStop { position: 0.03; color: colorTableItem.color1 }
            GradientStop { position: 0.2; color: "transparent" }
            GradientStop { position: 0.8; color: "transparent" }
            GradientStop { position: 0.97; color: colorTableItem.color2 }
            GradientStop { position: 1; color: "transparent" }
        }

        visible: false
    }

    ShaderEffectSource {
        id: colorTableSource
        sourceItem: colorTableItem
        smooth: true
    }

    Repeater {
        model: 4
        Swirl {

            width: parent.width
            anchors.bottom: parent.bottom
            height: parent.height / (2 + index)
            opacity: 0.3
            speed: (index + 1) / 5
            colorTable: colorTableSource
        }
    }


    ParticleSystem{
        id: particles
    }
    ImageParticle{
        anchors.fill: parent
        system: particles
        source: "images/particle.png"
        alpha: 0
        colorVariation: 0.2
    }
    Emitter{
        anchors.fill: parent
        system: particles
        emitRate: Math.sqrt(parent.width * parent.height) / 30
        lifeSpan: 2000
        emitting: true
        size: 4
        sizeVariation: 2

        acceleration: AngledDirection { angle: 90; angleVariation: 360; magnitude: 10; }
        speed: AngledDirection { angle: -90; angleVariation: 360; magnitude: 5; }
    }



}
