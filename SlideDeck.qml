import QtQuick 2.0
import Qt.labs.presentation 1.0

import EffectSlide 1.0
import samegame 1.0

AnimatedPresentation {
    width: 1280
    height: 720

    property color textColor: "white"

    Slide {
        centeredText: "QML Scene Graph"
        fontScale: 2

    }


    Slide {
        title: "QML Scene Graph"
        content: [
            "Rendering engine for QML 2 in Qt 5",
            "Retained graphics model",
            "OpenGL (ES) 2.0 based",
            "Runs on all OpenGL capable Lighthouse platforms"
        ]
    }


    Slide
    {
        title: "It works!"
        SameGame
        {
            anchors.centerIn: parent
            scale: parent.height / height
        }
    }


    TextSlide {}


    EffectSampleSlide { }
    EffectSlide {}


    PaintedItemSlide {}


    GLIntegrationSlide {}


    VSyncSlide {}


    ThreadingSlide {}


    Slide {
        fontScale: 1.5
        centeredText: "No C++ was used in this demo..."
    }


    Slide {
        fontScale: 1.5
        centeredText: "QML Scene Graph\n\nThank you for watching!"
    }


    Rectangle {
        id: slideCover
        color: "black"
        anchors.fill: parent

        visible: true

        SequentialAnimation {
            id: slideCoverFadeAnimation
            NumberAnimation { target: slideCover; property: "opacity"; from: 1; to: 0; duration: 2500 }
            PropertyAction { target: slideCover; property: "visible"; value: "false" }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: slideCoverFadeAnimation.running = true
        }
    }

}
