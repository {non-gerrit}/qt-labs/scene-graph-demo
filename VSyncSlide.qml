import QtQuick 2.0
import Qt.labs.presentation 1.0

Slide { // Vsync animations / flickable..
    title: "Animations Synchronized to VBlank"
    content: [
        "Advance animations exactly once per frame",
        "No more jerkies",
        "It's Velvet!"
    ]
    contentWidth: width / 2

    Flickable {
        id: vsyncFlickable
        x: parent.width / 2 + parent.baseFontSize
        width: parent.width / 2 - parent.baseFontSize
        height: parent.height

        clip: true

        contentHeight: 25000
        contentWidth: 25000

        Image {
            id: flickImage
            anchors.fill: parent
            smooth: true
            source: "images/landscape.png"
            fillMode: Image.Tile
        }
    }

    Text {
        font.pixelSize: parent.baseFontSize * 0.3
        color: "white"
        text: "Must be experienced first hand, not via video capture"
        anchors.top: vsyncFlickable.bottom
        anchors.horizontalCenter: vsyncFlickable.horizontalCenter
    }
}


