import Qt.labs.presentation 1.0

Slide
{
    title: "ShaderEffectItem Example"

    ShaderDemo
    {
        anchors.centerIn: parent
        scale: parent.width * 0.7 / width
    }
}
